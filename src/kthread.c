/*
 * Copyright (C) 2013-2017 Elboulangero (elboulangero@gmail.com)
 *
 * This file is part of irqtop.
 *
 * irqtop is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irqtop is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with irqtop. If not, see <http://www.gnu.org/licenses/>.
 */

/* ISO C */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* Local */
#include "log.h"
#include "kthread.h"

struct kthread *
kthread_dup(struct kthread *src)
{
	struct kthread *new;

	new = calloc(1, sizeof(struct kthread));
	if (!new) {
		error("Out of memory !");
		return NULL;
	}
	memcpy(new, src, sizeof(struct kthread));
	memset(&new->list, 0, sizeof(struct list_head));
	return new;
}

void
kthread_free(struct kthread *self)
{
	if (!self) {
		return;
	}

	debug("Free kthread [%p: %5u / %3u]", (void *) self, self->pid, self->irq);

	free(self);
}

struct kthread *
kthread_new(unsigned int pid)
{
	struct kthread *self;
	unsigned int irq_number;
	int kthread_prio;
	FILE *fp;
	int count;
	char stat_path[64];
	char buffer[1024];
	char *ptr;
	size_t i;

	/* Read stat file associated with this process */
	snprintf(stat_path, sizeof stat_path, "/proc/%u/stat", pid);
	fp = fopen(stat_path, "r");
	if (!fp) {
		error("Failed to open %s", stat_path);
		return NULL;
	}
	count = fread(buffer, sizeof(char), sizeof buffer, fp);
	fclose(fp);
	if (count < 0) {
		error("Failed to read %s", stat_path);
		return NULL;
	}
	if (count == sizeof buffer) {
		error("Buffer too short for %s !", stat_path);
		return NULL;
	}

	/* Check that this process is a kernel thread.
	 * We look at the second field in the stat file.
	 * Other methods exist to identify a kernel thread, see:
	 * http://stackoverflow.com/q/12213445
	 */
	ptr = strchr(buffer, '(');
	if (!ptr) {
		error("Invalid line !");
		return NULL;
	}
	ptr++;
	if (strncmp(ptr, "irq/", 4)) {
		return NULL;
	}

	/* Get IRQ number */
	ptr += 4;
	irq_number = strtoul(ptr, NULL, 10);
	ptr = strchr(ptr, ')');
	if (!ptr) {
		error("Invalid line !");
		return NULL;
	}
	ptr++;

	/* Now we just need to get the priority and we're all good.
	 * This is the 38th field from here.
	 */
	for (i = 0; i < 38; i++) {
		ptr = strchr(ptr, ' ');
		if (!ptr) {
			error("Invalid line !");
			return NULL;
		}
		ptr++;
	}
	kthread_prio = strtol(ptr, NULL, 10);

	/* Allocation */
	self = calloc(1, sizeof(struct kthread));
	if (!self) {
		return NULL;
	}

	/* Create kernel thread */
	self->pid = pid;
	self->prio = kthread_prio;
	self->irq = irq_number;

	debug("New kthread [%p: %5u / %3u]", (void *) self, self->pid, self->irq);
	return self;
}
