/*
 * Copyright (C) 2013-2017 Elboulangero <elboulangero@gmail.com>
 *
 * This file is part of irqtop.
 *
 * irqtop is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * irqtop is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with irqtop. If not, see <http://www.gnu.org/licenses/>.
 */

/* ISO C */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

/* Local */
#include "log.h"
#include "list.h"
#include "kthread.h"
#include "interrupt.h"

extern unsigned int cpu_count;

static void
cpu_init(struct cpu *self, unsigned long count)
{
	/* Init current counter */
	self->count.cur = count;

	/* Initialize minimum rate */
	self->rate.min = UINT_MAX;
}

static void
cpu_refresh(struct cpu *self, unsigned long count)
{
	unsigned long diff;

	/* Refresh counters */
	self->count.prev = self->count.cur;
	self->count.cur = count;

	/* Increment sum */
	diff = self->count.cur - self->count.prev;
	self->count.sum += diff;

	/* Refresh rates */
	self->rate.cur = diff;
	if (diff < self->rate.min) {
		self->rate.min = diff;
	}
	if (diff > self->rate.max) {
		self->rate.max = diff;
	}
}

void
interrupt_print_summary(struct interrupt *self, int total_only)
{
	size_t i;
	unsigned long iter_count;
	unsigned long min_rate;
	unsigned long mean_rate;
	struct kthread *kthread = self->kthread;
	struct cpu *total = &self->total;

	/* Display interrupt ID */
	printf("%s ", self->id);

	/* Display kthread stuff if any */
	if (kthread) {
		printf("%5u %3d ", kthread->pid, kthread->prio);
	} else {
		printf("    -   - ");
	}
	printf("| ");

	/* Display total rates.
	 * If min rate has not been initialized yet, we display 0.
	 */
	iter_count = self->iter_cur - self->iter_start;
	min_rate = total->rate.min == UINT_MAX ? 0 : total->rate.min;
	mean_rate = total->count.sum / iter_count;
	printf("%6lu %6lu %6lu ", min_rate, mean_rate, total->rate.max);

	/* Display rates per CPU */
	if (!total_only) {
		printf("| ");
		for (i = 0; i < cpu_count; i++) {
			mean_rate = self->cpu[i].count.sum / iter_count;
			printf("%6lu ", mean_rate);
		}
	}

	/* Display total count */
	printf("| %8lu | %s\n", total->count.sum, self->device);
}

void
interrupt_print(struct interrupt *self, int total_only)
{
	size_t i;
	unsigned long min_rate;
	struct kthread *kthread = self->kthread;
	struct cpu *total = &self->total;

	/* Display interrupt ID */
	printf("%s ", self->id);

	/* Display kthread stuff if any */
	if (kthread) {
		printf("%5u %3d ", kthread->pid, kthread->prio);
	} else {
		printf("    -   - ");
	}
	printf("| ");

	/* Display total rates.
	 * If min rate has not been initialized yet, we display 0.
	 */
	min_rate = total->rate.min == UINT_MAX ? 0 : total->rate.min;
	printf("%6lu %6lu %6lu ", min_rate, total->rate.cur, total->rate.max);

	/* Display rates per CPU */
	if (!total_only) {
		printf("| ");
		for (i = 0; i < cpu_count; i++) {
			printf("%6lu ", self->cpu[i].rate.cur);
		}
	}

	/* Display total count */
	printf("| %8lu | %s\n", total->count.sum, self->device);
}

int
interrupt_init(struct interrupt *self, const char *line, unsigned int iter_count)
{
	size_t i, len;
	char *ptr, *endptr;
	unsigned long sum;

	/* Initialize ID */
	ptr = strchr(line, ':');
	if (!ptr) {
		goto invalid_line;
	}
	len = ptr - line;
	if (len > sizeof self->id - 1) {
		goto invalid_line;
	}
	strncpy(self->id, line, len);
	ptr++;

	/* Initialize each CPU values */
	sum = 0;
	for (i = 0; i < cpu_count && ptr; i++) {
		struct cpu *cpu = &self->cpu[i];
		unsigned long count = strtoul(ptr, &endptr, 10);
		cpu_init(cpu, count);
		sum += count;
		ptr = endptr;
	}
	if (i != cpu_count) {
		goto invalid_line;
	}

	/* Initialize total values */
	{
		struct cpu *total = &self->total;
		unsigned long count = sum;
		cpu_init(total, count);
	}

	/* Initialize iteration count */
	self->iter_start = iter_count;

	/* Initialize device name */
	while (*ptr == ' ') {
		ptr++;
	}
	endptr = strchr(ptr, '\n');
	if (!endptr) {
		goto invalid_line;
	}
	len = endptr - ptr;
	if (len > sizeof self->device - 1) {
		goto invalid_line;
	}
	strncpy(self->device, ptr, len);

	return 0;

 invalid_line:
	error("Invalid line: %s", line);
	return -1;
}

void
interrupt_refresh(struct interrupt *self, const char *line, unsigned int iter_count)
{
	size_t i;
	char *ptr, *endptr;
	unsigned long sum;

	/* Set ptr to the first value */
	ptr = strchr(line, ':');
	if (!ptr) {
		return;
	}
	ptr++;

	/* Refresh each CPU */
	sum = 0;
	for (i = 0; i < cpu_count && ptr; i++) {
		struct cpu *cpu = &self->cpu[i];
		unsigned long count = strtoul(ptr, &endptr, 10);
		cpu_refresh(cpu, count);
		sum += count;
		ptr = endptr;
	}

	/* Refresh total */
	{
		struct cpu *total = &self->total;
		unsigned long count = sum;
		cpu_refresh(total, count);
	}

	/* Set current iteration count */
	self->iter_cur = iter_count;
}

void
interrupt_free(struct interrupt *self)
{
	if (!self) {
		return;
	}

	debug("Free interrupt [%p: %s]", (void *) self, self->id);

	kthread_free(self->kthread);
	free(self->cpu);
	free(self);
}

struct interrupt *
interrupt_new(const char *line, struct list_head *kthread_list, unsigned int iter_count)
{
	struct interrupt *self;
	char *endptr;
	unsigned int interrupt_number;

	/* Allocations */
	self = calloc(1, sizeof(struct interrupt));
	if (!self) {
		goto out_of_memory;
	}
	self->cpu = calloc(cpu_count, sizeof(struct cpu));
	if (!self->cpu) {
		goto out_of_memory;
	}

	/* Initialize interrupt content */
	if (interrupt_init(self, line, iter_count)) {
		goto init_failure;
	}

	/* If the iteration counter is not zero, it means that the interrupt
	 * appeared during the execution of the program. In this case, we know
	 * that the previous counter values were zero. And we want to start
	 * computing the rates right now, otherwise we will miss these very first
	 * interruptions !
	 */
	if (iter_count != 0) {
		/* Re-initialize the CPU counters to zero, because we know that
		 * it was the previous counter values for this interrupt.
		 */
		size_t i;
		for (i = 0; i < cpu_count; i++) {
			cpu_init(&self->cpu[i], 0);
		}
		cpu_init(&self->total, 0);
		/* Change iteration start count */
		self->iter_start = iter_count - 1;
		/* Do a first refresh right now */
		interrupt_refresh(self, line, iter_count);
	}

	/* Determine the type of this interrupt. In case it's an IRQ,
	 * look for a kernel thread that could be associated with it.
	 */
	interrupt_number = strtoul(self->id, &endptr, 10);
	if (*endptr != '\0') {
		self->type = INT_INTERNAL;
	} else {
		struct kthread *entry;
		self->type = INT_IRQ;
		list_for_each_entry(entry, kthread_list, list) {
			if (entry->irq == interrupt_number) {
				debug("Interrupt %u has a kernel thread", interrupt_number);
				self->kthread = kthread_dup(entry);
				if (!self->kthread) {
					goto out_of_memory;
				}
				break;
			}
		}
		if (self->kthread == NULL) {
			debug("Interrupt %u has no kernel thread", interrupt_number);
		}
	}

	/* Finished */
	debug("New interrupt [%p: %s]", (void *) self, self->id);
	return self;

 init_failure:
	error("Failed to init interrupt !");
	interrupt_free(self);
	return NULL;

 out_of_memory:
	error("Out of memory !");
	interrupt_free(self);
	return NULL;
}
