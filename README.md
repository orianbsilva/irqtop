Irqtop is a simple monitor for `/proc/interrupts`.

A Debian package available at <http://pkg.elboulangero.com/debian/>.

Build
-----

Pretty build

	make

Verbose build

	V=1 make

Installation (run as root)
--------------------------

Default install directory

	make install

Custom install directory

	PREFIX=/usr make install

Uninstall

	make uninstall

Help
----

Short help

	irqtop -h

Detailed help

	man irqtop

